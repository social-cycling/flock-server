# Flock Server

This program is the server component of the [Social Cycling](https://gitlab.com/rgarcia-herrera/social-cycling) mobile app.


## Installation and running

Clone or download this repo.
Install Python libraries, like so:

	$ pip install -r requirements.txt
	[...]

### MongoDB setup

[MongoDB](https://www.mongodb.com/) is used for data
persistence. Geographical queries require the creation of indexes for
the **point** and **destination** fields.

	$ mongodb
	[...]
	> use test
	switched to db test
	> db.bike.createIndex({point:"2dsphere"});
	[...]
	> db.bike.createIndex({destination:"2dsphere"});
	[...]


### Development webserver

In a production environment Flask apps must be deployed with a
dedicated webserver. Instructions for doing so are documented
[elsewhere](http://flask.pocoo.org/docs/1.0/deploying/).

For simplicity the app may be run with the included development
server, using the following commands:

	$ export FLASK_APP=server.py
	$ flask run
	* Serving Flask app "server.py"
	[...]
	* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)


## Instance at LANCIS

We're hosting an instance of this server at

http://flock.apps.lancis.ecologia.unam.mx/

