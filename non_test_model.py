import pytest
import model

from mongoengine import connect
connect('test')


juarez = (-98.88038, 19.50963)
# scope='module'
@pytest.fixture()
def four_bikers():
    """
    create four bikers with same destination
    three of them closeby, one too far to hive with
    """
    r = model.Bike(point=(-98.87011, 19.48790),
                   destination=juarez)
    r.save()
    
    k = model.Bike(point=(-98.87086, 19.48838),
                   destination=juarez)
    k.save()
    
    m = model.Bike(point=(-98.87004, 19.48737),
                   destination=juarez)
    m.save()
    
    t = model.Bike(point=(-98.87312, 19.48658),
                   destination=juarez)
    t.save()
    
    yield [r, k, m, t]

    # teardown
    model.Bike.objects.delete()


def test_flock_data(four_bikers):
    r = four_bikers[0]
    assert r.flock_data() ==  {'flock_avg_speed': 0.0,
                               'flock_distance': 23.86718565123822,
                               'flock_heading': -94.43342715427826,
                               'flock_size': 3}


# http://localhost:5000/register/?dest_lat=19.50963&dest_lon=-98.88038
# http://localhost:5000/update/5c4239198849af1ee43edb2c/?lon=-98.87011&lat=19.48790
