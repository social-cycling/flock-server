from flask import Flask
from flask import request, jsonify
from flask_mongoengine import MongoEngine
from model import Bike

app = Flask(__name__)

db = MongoEngine(app)


# TODO: at registration give current location
@app.route("/register/")
def register():
    dest_lon = request.args.get('dest_lon', None)
    dest_lat = request.args.get('dest_lat', None)
    client_id = request.args.get('client_id', None)

    assert dest_lat is not None and dest_lon is not None and client_id is not None

    dest = (float(dest_lon), float(dest_lat))

    bike = Bike(point=(0, 0),
                destination=dest,
                client_id=client_id)

    bike.save()

    return jsonify(bike_id=str(bike.id))


# TODO: recieve gps bearing and speed
@app.route("/update/<bike_id>/")
def update(bike_id):

    bike = Bike.objects.get(id=bike_id)

    lat = float(request.args.get('lat', None))
    lon = float(request.args.get('lon', None))
    speed = float(request.args.get('speed', None))
    bearing = float(request.args.get('bearing', None))
    client_id = request.args.get('client_id', None)

    # TODO: maybe client tunes these?
    # local_altruism = float(request.args.get('local_altruism', 0.1))
    # dest_altruism = float(request.args.get('dest_altruism', 0.2))

    bike.update(lat, lon, speed, bearing, client_id)

    return jsonify(last_update=bike.last_update.isoformat(),
                   **bike.flock_data())
